# EPAM 201.1 Materials Masking and Functions

Developed with Unreal Engine 5

## Task Description: 

The main goal of this exercise is to learn how to utilize Material Functions and how to use Masks inside the Materials. 

Please use any sets of the Textures that are to your liking and try to experiment with different kinds of the textures! 

## List of assets that should be present in the Project’s Content Folder: 

1. Master Material that uses Material Functions 
2. Material Instance of the Material with Functions 
3. Master Material that uses masks 
4. Water Caustics Material Function 
5. Master Material that uses Water Caustics Material Function 

### Master Material that uses Material Functions requirements: 

1. Surface Opaque Material 
2. Blend overlay node 
3. Switch Parameter node 
4. FlattenNormal node 
5. Material inputs utilized: Base Color, Metallic, Specular, Roughness, Normal 

### Instance of the Material that uses Material Functions requirements: 

1. Material should not resemble the Master Material in any way 

### Master Material that uses masks requirements: 

1. Surface Opaque Material 
2. Textures are used to mask off parts of the surface 
3. Material inputs utilized: Base Color, Metallic, Roughness, Normal 

### Water Caustics Material Function requirements: 

1. MF has the following inputs: Coordinates (V2) Result, Speed (S), Devisor (S), Texture (T2d) 

### Master Material that uses Water Caustics Material Function requirements: 

1. Material Domain is set to Light Function
2. Water Caustics Material Function is used and connected to Emissive Color 

![Result.](/readme/EPAM_201_1_Mat_Mask.gif "Result.")